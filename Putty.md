To find a bash on your Windows computer, you better install putty and start remotely right away.
Download putty from https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
Next, you need to create a public-private key pair and have somebody install it on a Linux computer.